# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tlovato <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/02/20 10:01:12 by tlovato           #+#    #+#              #
#    Updated: 2016/07/25 14:29:40 by tlovato          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


NAME = libftprintf.a

CC = gcc
CFLAGS = 

SRC_PATH = ./src/
INC_PATH = ./inc/
LIBFT_PATH = ./libft/

LIBFT = libft.a

SRC_FILES = attrib_case.c convtype.c flags.c flags2.c\
ft_printf.c invalid_arg.c llitoa.c llitoa_base.c modifs.c\
pars_flags.c parsing.c\
print_conv.c ullitoa.c\
ullitoa_base.c utils.c utils2.c utils3.c utils4.c wide_conversion.c

SRC = $(addprefix $(SRC_PATH), $(SRC_FILES))

OBJ_PATH = ./obj/
OBJ_O = $(SRC_FILES:.c=.o)
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_O))

INC = $(INC_PATH)
INC_CC = $(foreach DIR, $(INC), -I$(DIR))
CFLAGS += $(INC_CC)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir -p $(OBJ_PATH)
	@$(CC) $(CFLAGS) -o $@ -c $<

all : $(LIBFT_PATH)$(LIBFT) $(NAME)

$(LIBFT_PATH)$(LIBFT):
	@$(MAKE) -C $(LIBFT_PATH)

$(NAME) : $(OBJ)
	@cp $(LIBFT_PATH)$(LIBFT) $(NAME)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

clean :
	rm -f $(OBJ)

fclean : clean
	rm -f $(NAME)	

re : fclean all

.PHONY : all clean fclean re
